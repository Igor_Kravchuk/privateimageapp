import UIKit

class ImagesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    
    func config(with object: AppImage?){
        guard let imageName = object?.imageName else {
            return
        }
        let image = ImageManager.shared.loadImage(fileName: imageName)
        self.photoImageView.image = image
    }
}
