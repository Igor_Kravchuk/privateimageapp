import Foundation
import UIKit

class ImageManager {
    
    static let shared = ImageManager()
    
    func saveImage(image: UIImage) -> String? {
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil}
        let fileName = UUID().uuidString
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        guard let data = image.jpegData(compressionQuality: 1) else { return nil}
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }
        }
        do {
            try data.write(to: fileURL)
            return fileName
        } catch let error {
            print("error saving file with error", error)
            return nil
        }
    }
    
    func loadImage(fileName: String) -> UIImage? {
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let imageUrl = documentsDirectory.appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
        }
        return nil
    }
    
    func loadAppImages() -> [AppImage] {
        guard let appImages = UserDefaults.standard.value([AppImage].self, forKey: Keys.appImages.rawValue) else {
            return []
        }
        return appImages
    }
    
    func loadPictures(appImages: [AppImage]?) -> [UIImage] {
        guard let appImages = appImages else {
            return []
        }
        var pictures: [UIImage] = []
        for imageIndex in 0...appImages.count {
            if let imageName = appImages[imageIndex].imageName {
                let image = self.loadImage(fileName: imageName)
                if let image = image {
                    pictures.append(image)
                }
            }
        }
        return pictures
    }
    
    private init() {}
}
