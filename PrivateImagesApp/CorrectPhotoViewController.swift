import UIKit
//MARK: - ENUMS
enum changePictureDirection {
    case forward
    case back
}

class CorrectPhotoViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var staticImageViewOnScreen: UIImageView?
    @IBOutlet weak var imageViewArea: UIView?
    @IBOutlet weak var appImageView: UIView?
    @IBOutlet var favouriteButton: UIButton?
    @IBOutlet weak var commentField: UITextView?
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint?
    @IBOutlet weak var topViewConstraint: NSLayoutConstraint?
    @IBOutlet weak var backButton: UIButton?
    @IBOutlet weak var scrollView: UIScrollView?
    
    //MARK: - VARS
    private var startPointX = CGFloat()
    private var startPointY = CGFloat()
    private var pictureHeight = CGFloat()
    private var pictureWidth = CGFloat()
    private var pickedImages: [AppImage]?
    private var movingImageView: UIImageView?
    var pictureOnScreenNumber: Int = 0
    private var startAppImage: AppImage?
    private var endAppImage: AppImage?
    
    //MARK: - LETS
    private let interval = 1.0
    
    //MARK: - Life functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addButtonCornerRadius()
        self.pickedImages = ImageManager.shared.loadAppImages()
        self.addFirstPicture()
        self.registerForKeyboardNotifications()
        self.addSwipeGestureRecognizer(view: self.view)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.makeImageSettings()
    }
    
    //MARK: - IBActions
    @IBAction private func moveToNextItem(_ sender:UISwipeGestureRecognizer) {
        switch sender.direction{
        case .left:
            self.swipe(direction: .forward)
        case .right:
            self.swipe(direction: .back)
        default: break
        }
    }
    
    @IBAction func goToPreviousViewController(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction private func pressFavouriteButton(_ sender: UIButton) {
        self.pressFavouriteButton()
    }
    
    @IBAction private func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
              let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
              let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
              let bottomViewConstraint = self.bottomViewConstraint,
              let topViewConstraint = self.topViewConstraint
        else { return }
        if notification.name == UIResponder.keyboardWillHideNotification {
            bottomViewConstraint.constant = 0
            topViewConstraint.constant = 0
            self.safeComment()
        } else {
            bottomViewConstraint.constant = keyboardScreenEndFrame.height + 10
            topViewConstraint.constant = -(keyboardScreenEndFrame.height + 10)
        }
        self.view.needsUpdateConstraints()
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK: - Flow functions
    func addButtonCornerRadius() {
        guard let button = self.backButton else { return }
        let radius = button.frame.height / 2
        button.layer.cornerRadius = radius
    }
    
    private func makeImageSettings() {
        guard let staticImageViewOnScreen = self.staticImageViewOnScreen else { return }
        self.pictureWidth = staticImageViewOnScreen.frame.width
        self.pictureHeight = staticImageViewOnScreen.frame.height
    }
    
    private func addFirstPicture() {
        guard let pickedImages = self.pickedImages else { return }
        self.startAppImage = pickedImages[self.pictureOnScreenNumber]
        if let imageName = pickedImages[self.pictureOnScreenNumber].imageName {
            self.staticImageViewOnScreen?.image = ImageManager.shared.loadImage(fileName: imageName)
            let comment = pickedImages[self.pictureOnScreenNumber].comment
            self.commentField?.text = comment
        }
        self.staticImageViewOnScreen?.dropShadow(0)
        self.checkFavouriteImage()
    }
    
    private func addNextImageView(startX: CGFloat, nextPictureNumber: Int) {
        guard let pickedImages = self.pickedImages,
              let staticImageViewOnScreen = self.staticImageViewOnScreen
        else { return }
        self.movingImageView = UIImageView()
        self.movingImageView?.contentMode = self.staticImageViewOnScreen?.contentMode ?? .scaleToFill
        self.movingImageView?.backgroundColor = self.staticImageViewOnScreen?.backgroundColor
        if let movingImageView = self.movingImageView {
            let pointY = staticImageViewOnScreen.frame.origin.y
            movingImageView.frame = CGRect(x: startX, y: pointY, width: self.pictureWidth, height: self.pictureHeight)
            if let imageName = pickedImages[nextPictureNumber].imageName {
                movingImageView.image = ImageManager.shared.loadImage(fileName: imageName)
            }
            movingImageView.dropShadow(0)
            self.imageViewArea?.addSubview(movingImageView)
        }
    }
    
    private func swipe(direction: changePictureDirection) {
        self.checkAppImageChanges(direction: direction)
        guard let nextImageView = self.movingImageView,
              let pickedImages = self.pickedImages
        else { return }
        self.checkFavouriteImage()
        self.commentField?.text = pickedImages[self.pictureOnScreenNumber].comment
        UIView.animate(withDuration: self.interval) {
            nextImageView.frame.origin.x = self.staticImageViewOnScreen?.frame.origin.x ?? 0
        } completion: { (_) in
            if let imageName = pickedImages[self.pictureOnScreenNumber].imageName
            {
                self.staticImageViewOnScreen?.image = ImageManager.shared.loadImage(fileName: imageName)
            }
            nextImageView.removeFromSuperview()
        }
    }
    
    private func countNextPictureNumber(direction: changePictureDirection) -> Int {
        var nextPictureNumber = self.pictureOnScreenNumber
        if let pickedImages = self.pickedImages {
            switch direction {
            case .forward:
                nextPictureNumber += 1
                if nextPictureNumber > pickedImages.count - 1 {
                    nextPictureNumber = pickedImages.startIndex
                }
            case .back:
                nextPictureNumber -= 1
                if nextPictureNumber < 0 {
                    nextPictureNumber = pickedImages.count - 1
                }
            }
        }
        return nextPictureNumber
    }
    
    private func checkFavouriteImage() {
        guard let pickedImages = self.pickedImages,
              let favouriteButton = self.favouriteButton
        else { return }
        if pickedImages[self.pictureOnScreenNumber].favourite == true {
            favouriteButton.isSelected = true
        } else {
            favouriteButton.isSelected = false
        }
    }
    
    private func pressFavouriteButton() {
        guard let pickedImages = self.pickedImages,
              let favouriteButton = self.favouriteButton
        else { return }
        if pickedImages[self.pictureOnScreenNumber].favourite == true {
            pickedImages[self.pictureOnScreenNumber].favourite = false
            favouriteButton.isSelected = false
            self.resafeAppImagesAfterEditing()
        } else {
            pickedImages[self.pictureOnScreenNumber].favourite = true
            favouriteButton.isSelected = true
            self.resafeAppImagesAfterEditing()
        }
    }
    
    private func resafeAppImagesAfterEditing() {
        UserDefaults.standard.set(encodable: self.pickedImages, forKey: Keys.appImages.rawValue)
    }
    
    func countPointXBehindPhone(direction: changePictureDirection) -> CGFloat {
        switch direction {
        case .forward:
            return self.view.frame.width
        case .back:
            return -self.pictureWidth
        }
    }
    
    private func addPictureBehindPhone(direction: changePictureDirection) {
        let nextPictureNumber = self.countNextPictureNumber(direction: direction)
        let pointXBehindPhone = self.countPointXBehindPhone(direction: direction)
        self.addNextImageView(startX: pointXBehindPhone, nextPictureNumber: nextPictureNumber)
        self.pictureOnScreenNumber = nextPictureNumber
    }
    
    private func checkAppImageChanges(direction: changePictureDirection) {
        switch direction {
        case .forward:
            self.addPictureBehindPhone(direction: .forward)
        case .back:
            self.addPictureBehindPhone(direction: .back)
        }
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func safeComment() {
        guard let pickedImages = self.pickedImages,
              let commentField = self.commentField
        else { return }
        pickedImages[self.pictureOnScreenNumber].comment = commentField.text
        UserDefaults.standard.set(encodable: self.pickedImages, forKey: Keys.appImages.rawValue)
    }
    
    private func addSwipeGestureRecognizer(view: UIView) {
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(moveToNextItem(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(moveToNextItem(_:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)
    }
    
}

extension CorrectPhotoViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return true
        }
        return true
    }
}
