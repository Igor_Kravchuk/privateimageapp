import Foundation

class AppImage: Codable {
    
    var imageName: String?
    var comment: String?
    var favourite: Bool?
    
    init(imageName: String?, comment: String?, favourite: Bool?) {
        self.imageName = imageName
        self.comment = comment
        self.favourite = favourite
    }
    
    private enum CodingKeys: String, CodingKey {
        case imageName
        case comment
        case favourite
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        imageName = try container.decodeIfPresent(String.self, forKey: .imageName)
        comment = try container.decodeIfPresent(String.self, forKey: .comment)
        favourite = try container.decodeIfPresent(Bool.self, forKey: .favourite)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.imageName, forKey: .imageName)
        try container.encode(self.comment, forKey: .comment)
        try container.encode(self.favourite, forKey: .favourite)
    }
    
}

