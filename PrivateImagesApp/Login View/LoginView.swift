import UIKit
import SwiftyKeychainKit

//MARK: - ENUMS
enum NextController {
    case CorrectController
    case SpyController
    case WrongController
}

protocol LoginViewDelegate: AnyObject {
    func goToCorrectViewController()
    func goToSpiesViewController()
}


class LoginView: UIView {
    //MARK: - IBOutlets
    @IBOutlet weak var alertArea: UIView?
    @IBOutlet weak var passwordField: UITextField?
    @IBOutlet weak var passwordLabel: UILabel?
    @IBOutlet weak var confirmButton: UIButton?
    @IBOutlet weak var showHideButton: UIButton?
    
    //MARK: - VARS
    weak var delegate: LoginViewDelegate?
    private var correctPassword: String?
    private var spyPassword: String?
    
    //MARK: - LETS
    let passwordKeychain = Keychain(service: "com.iKravchuk.PrivateImagesApp")
    let correctPasswordKey = KeychainKey<String>(key: "correctPasswordAccessToken")
    let spyPasswordKey = KeychainKey<String>(key: "spyPasswordAccessToken")
    
    //MARK: - IBActions
    @IBAction func pressShowHideButton(_ sender: UIButton) {
        self.showHidePassword()
    }
    
    @IBAction func pressConfirmButton(_ sender: UIButton) {
        self.checkPassword()
        self.passwordField?.text = ""
    }
    
    //MARK: - Functions
    class func instanceFromNib() -> LoginView {
        guard let loginView = UINib(nibName: "LoginView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? LoginView else {
            return LoginView()
        }
        return loginView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.confirmButton?.layer.cornerRadius = 15
        self.showHideButton?.layer.cornerRadius = 15
        self.alertArea?.layer.cornerRadius = 15
        self.passwordField?.isSecureTextEntry = true
        self.loadPasswords()
    }
    
    private func loadPasswords() {
        do {
            self.correctPassword = try passwordKeychain.get(self.correctPasswordKey)
            self.spyPassword = try passwordKeychain.get(self.spyPasswordKey)
        } catch let error {
            debugPrint(error)
        }
    }
    
    private func showHidePassword() {
        self.passwordField?.isSecureTextEntry = !(self.passwordField?.isSecureTextEntry ?? true)
        self.showHideButton?.isSelected = !(self.showHideButton?.isSelected ?? false)
    }
    
    func checkPassword()  {
        guard let correctPassword = self.correctPassword,
              let spyPassword = self.spyPassword
        else {
            return
        }
        let writtenPassword = self.passwordField?.text
        if correctPassword == writtenPassword {
            self.delegate?.goToCorrectViewController()
            self.removeFromSuperview()
        } else if spyPassword == writtenPassword {
            self.delegate?.goToSpiesViewController()
            self.removeFromSuperview()
        } else {
            self.passwordField?.text = ""
            self.passwordLabel?.text = "Try again"
        }
    }
}
