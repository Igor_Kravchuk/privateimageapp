import Foundation

import UIKit

extension UIView {
    
    func dropShadow(_ radius: CGFloat = 15) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 10, height: 10)
        self.layer.shadowRadius = 20
        self.layer.cornerRadius = radius
        self.layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        self.layer.shouldRasterize = true
    }
    
}
