import UIKit
//MARK: - ENUMS
enum Keys: String {
    case appImages = "App Images"
}

enum SourceType {
    case photoLibrary
    case camera
}

class CorrectViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var photoCollectionView: UICollectionView?
    @IBOutlet var mainButtons: [UIButton]?
    
    //MARK: - VARS
    var appImages: [AppImage]?
    
    //MARK: - LETS
    
    //MARK: - Life functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addButtonsCornerRadius()
        self.appImages = ImageManager.shared.loadAppImages()
    }
    
    //MARK: - IBActions
    @IBAction func addNewPicture(_ sender: UIButton) {
        self.showAlert()
    }
    
    @IBAction func goToPreviousViewController(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Flow functions
    func showAlert() {
        let alert = UIAlertController(title: "Choose a method", message: "", preferredStyle: .actionSheet)
        let album = UIAlertAction(title: "Album", style: .default) { (_) in
            self.pickImage(from: .photoLibrary)
        }
        let camera = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.pickImage(from: .camera)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(album)
        alert.addAction(camera)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    func addButtonsCornerRadius() {
        guard let buttons = self.mainButtons else { return }
        for button in buttons {
            let radius = button.frame.height / 2
            button.layer.cornerRadius = radius
        }
    }
    
    private func pickImage(from sourceType: SourceType) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.modalPresentationStyle = .currentContext
        imagePicker.allowsEditing = false
        switch sourceType {
        case .camera:
            imagePicker.sourceType = .camera
        case .photoLibrary:
            imagePicker.sourceType = .photoLibrary
        }
        present(imagePicker, animated: true, completion: nil)
    }
    
    private func saveImage(imageName: String) {
        var appImages: [AppImage] = []
        let currentImage = AppImage(imageName: imageName, comment: nil, favourite: false)
        if let previousAppImages = UserDefaults.standard.value([AppImage].self, forKey: Keys.appImages.rawValue) {
            appImages = previousAppImages
            appImages.append(currentImage)
        } else {
            appImages.append(currentImage)
        }
        UserDefaults.standard.set(encodable: appImages, forKey: Keys.appImages.rawValue)
    }
}

extension CorrectViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            if let pickedImageName = ImageManager.shared.saveImage(image: pickedImage) {
                self.saveImage(imageName: pickedImageName)
            }
            self.appImages = ImageManager.shared.loadAppImages()
            self.photoCollectionView?.reloadData()
            picker.dismiss(animated: true, completion: nil)
        }
    }
}

extension CorrectViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let appImages = self.appImages {
            return appImages.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagesCollectionViewCell", for: indexPath) as? ImagesCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.config(with: self.appImages?[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side: CGFloat = (view.frame.width - 10) / 2
        
        return CGSize(width: side, height: side)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let nextController = self.storyboard?.instantiateViewController(withIdentifier: "CorrectPhotoViewController") as? CorrectPhotoViewController else { return }
        nextController.pictureOnScreenNumber = indexPath.item
        self.navigationController?.pushViewController(nextController, animated: true)
    }
    
}


