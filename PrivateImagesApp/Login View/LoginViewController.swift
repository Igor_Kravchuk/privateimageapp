import UIKit
import SwiftyKeychainKit

//MARK: - ENUMS

class LoginViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var mainScrollView: UIScrollView?
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint?
    @IBOutlet weak var topViewConstraint: NSLayoutConstraint?
    
    //MARK: - Vars
    var loginView = LoginView()
    var screenWidth: CGFloat?
    var screenHeight: CGFloat?
    
    //MARK: - LETS
    let passwordKeychain = Keychain(service: "com.iKravchuk.PrivateImagesApp")
    let correctPasswordKey = KeychainKey<String>(key: "correctPasswordAccessToken")
    let spyPasswordKey = KeychainKey<String>(key: "spyPasswordAccessToken")
    
    //MARK: - Life functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.removeWhiteLineUnderScrollView()
        self.addPasswords()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.determineCharacteristics()
        self.addLoginView()
    }
    
    //MARK: - IBActions
    @IBAction private func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
              let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
              let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        if notification.name == UIResponder.keyboardWillHideNotification {
            bottomViewConstraint?.constant = 0
        } else {
            bottomViewConstraint?.constant = keyboardScreenEndFrame.height + 10
        }
        self.view.needsUpdateConstraints()
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK: - Flow functions
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func removeWhiteLineUnderScrollView() {
        self.automaticallyAdjustsScrollViewInsets = false
        self.mainScrollView?.contentInset = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
    }
    
    func determineCharacteristics() {
        self.screenWidth = self.view.frame.width
        self.screenHeight = self.view.frame.height
    }
    
    func addPasswords() {
        do {
            try self.passwordKeychain.set("123", for : self.correctPasswordKey)
            try self.passwordKeychain.set("12345", for : self.spyPasswordKey)
        } catch let error {
            debugPrint(error)
        }
    }
    
    func addLoginView() {
        guard let screenWidth = self.screenWidth,
              let screenHeight = self.screenHeight else { return }
        self.loginView = LoginView.instanceFromNib()
        let loginViewWidth = screenWidth * 3 / 4
        let loginViewHeight = screenHeight * 1 / 4
        self.loginView.frame = CGRect(x: 0, y: 0, width: loginViewWidth, height: loginViewHeight)
        self.loginView.center = self.mainScrollView?.center ?? CGPoint(x: self.view.frame.width / 2, y: self.view.frame.height / 2)
        self.loginView.delegate = self
        self.loginView.passwordField?.delegate = self
        self.mainScrollView?.addSubview(self.loginView)
    }
    
}

extension LoginViewController: LoginViewDelegate {
    func goToSpiesViewController() {
        guard let nextController = self.storyboard?.instantiateViewController(withIdentifier: "SpiesViewController") as? SpiesViewController else { return }
        self.navigationController?.pushViewController(nextController, animated: true)
    }
    
    func goToCorrectViewController() {
        guard let nextController = self.storyboard?.instantiateViewController(withIdentifier: "CorrectViewController") as? CorrectViewController else { return }
        self.navigationController?.pushViewController(nextController, animated: true)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.loginView.checkPassword()
        return true
    }
}
