import UIKit
    //MARK: - ENUMS

class SpiesViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var backButton: UIButton?
    
    //MARK: - VARS
    
    //MARK: - LETS
    
    //MARK: - Life functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addButtonCornerRadius()
    }
    
    //MARK: - IBActions
    @IBAction func goToPreviousViewController(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Flow functions
    func addButtonCornerRadius() {
        guard let button = self.backButton else { return }
            let radius = button.frame.height / 2
            button.layer.cornerRadius = radius
    }
    
}
